package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int startOfGeometricProgression = scanner.nextInt();
        int stepOfGeometricProgression = scanner.nextInt();
        int number = scanner.nextInt();
        int findSum = findSumGeometricProgression(startOfGeometricProgression, stepOfGeometricProgression, number);
        int findN = findNinGeometricProgression(startOfGeometricProgression, stepOfGeometricProgression, number);
        System.out.println(findSum);
        System.out.println(findN);
    }

    public static int findSumGeometricProgression(int startOfGeometricProgression, int stepOfGeometricProgression, int number) {
        if (number == 1) {
            return startOfGeometricProgression;
        }
        return findSumGeometricProgression(startOfGeometricProgression * stepOfGeometricProgression, stepOfGeometricProgression, number - 1) + startOfGeometricProgression;
    }

    public static int findNinGeometricProgression(int startOfGeometricProgression, int stepOfGeometricProgression, int number) {
        if (number == 1) {
            return startOfGeometricProgression;
        }
        return findNinGeometricProgression(startOfGeometricProgression, stepOfGeometricProgression, number - 1) * stepOfGeometricProgression;
    }
}