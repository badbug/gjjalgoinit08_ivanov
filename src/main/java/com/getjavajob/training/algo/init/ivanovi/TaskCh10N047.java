package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int fibonacciN = findFibonacci(number);
        System.out.println(fibonacciN);
    }

    public static int findFibonacci(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return findFibonacci(n - 1) + findFibonacci(n - 2);
    }
}