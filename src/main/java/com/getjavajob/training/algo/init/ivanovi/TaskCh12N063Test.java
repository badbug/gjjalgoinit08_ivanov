package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh12N063.*;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        avgPupilCountFuncTest();
    }

    public static void avgPupilCountFuncTest() {
        int[][] array = new int[][]{
                {10, 11, 13, 12},
                {11, 12, 13, 14},
                {11, 11, 11, 11},
                {12, 14, 16, 20},
                {22, 25, 25, 27},
                {19, 16, 21, 16},
                {12, 11, 15, 12},
                {11, 12, 13, 14},
                {24, 23, 22, 21},
                {12, 14, 16, 20},
                {22, 25, 25, 27}
        };
        double[] expectedArray = {11.5, 12.5, 11.0, 15.5, 24.75, 18.0, 12.5, 12.5, 22.5, 15.5, 24.75};
        assertEquals("TaskCh12N063Test.avgPupilCountFuncTest", expectedArray, avgPupilCountFunc(array));
    }
}