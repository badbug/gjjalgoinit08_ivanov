package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {
        int[][] array1 = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5, 5},
                {6, 6, 6, 6, 6, 6}
        };
        int[][] array2 = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6}
        };
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter row number to delete from 1 to " + (array1.length) + ": ");
        int rowToDelete = scanner.nextInt();
        System.out.print("Enter column number to delete from 1 to " + (array2[1].length) + ": ");
        int colToDelete = scanner.nextInt();
        System.out.println();
        int[][] arrayWithDeletedRow = deleteRowInArray(array1, rowToDelete);
        int[][] arrayWithDeletedCol = deleteColInArray(array2, colToDelete);
        printMultyArray(arrayWithDeletedRow);
        printMultyArray(arrayWithDeletedCol);
    }

    public static int[][] deleteRowInArray(int[][] array, int rowToDelete) {
        for (int i = rowToDelete - 1; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == array.length - 1) {
                    array[i][j] = 0;
                } else {
                    array[i][j] = array[i + 1][j];
                }
            }
        }
        return array;
    }

    public static int[][] deleteColInArray(int[][] array, int colToDelete) {
        for (int i = 0; i < array.length; i++) {
            for (int j = colToDelete - 1; j < array[1].length; j++) {
                if (j == array[1].length - 1) {
                    array[i][j] = 0;
                } else {
                    array[i][j] = array[i][j + 1];
                }
            }
        }
        return array;
    }

    public static void printMultyArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}