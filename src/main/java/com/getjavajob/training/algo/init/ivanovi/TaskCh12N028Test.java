package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh12N028.*;

public class TaskCh12N028Test {
    public static void main(String[] args) {
        fillArrayTest();
    }

    public static void fillArrayTest() {
        int[][] array = new int[5][5];
        int[][] expectedArray = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}};
        assertEquals("TaskCh12N028Test.fillArrayTest", expectedArray, fillArray(array));
    }
}