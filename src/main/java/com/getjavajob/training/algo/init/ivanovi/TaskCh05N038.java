package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh05N038 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTurns = scanner.nextInt();
        double[] distance = calcDistance(numberOfTurns);
        System.out.println("Full distance = " + distance[0] + ", Distance from home = " + distance[1]);
    }

    public static double[] calcDistance(int numberOfTurns) {
        double[] distance = new double[2];
        double fullDistance = 0;
        double distanceFromHome = 0;
        for (int i = 1; i <= numberOfTurns; i++) {
            fullDistance += 1.0 / i;
            distanceFromHome = i % 2 == 0 ? (distanceFromHome -= 1.0 / i) : (distanceFromHome += 1.0 / i);
        }
        distance[0] = Math.round(fullDistance * 100.0) / 100.0;
        distance[1] = Math.round(distanceFromHome * 100.0) / 100.0;
        return distance;
    }
}