package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh04N036.*;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        checkTrafficLightColorTest1();
        checkTrafficLightColorTest2();
    }

    public static void checkTrafficLightColorTest1() {
        assertEquals("TaskCh04N036Test.checkTrafficLightColorTest1", "red", checkTrafficLightColor(3));
    }

    public static void checkTrafficLightColorTest2() {
        assertEquals("TaskCh04N036Test.checkTrafficLightColorTest2", "green", checkTrafficLightColor(5));
    }
}