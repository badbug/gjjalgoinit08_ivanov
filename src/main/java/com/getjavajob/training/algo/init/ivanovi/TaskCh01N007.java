package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

import static java.lang.Math.*;

public class TaskCh01N007 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        double x = scanner.nextDouble();
        double resultP = taskP(a, b, c, x);
        double resultO = taskO(x);
        double resultR = taskR(x);
        double resultS = taskS(x);
        System.out.println(resultO);
        System.out.println(resultP);
        System.out.println(resultR);
        System.out.println(resultS);
    }

    public static double taskO(double x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    public static double taskP(double a, double b, double c, double x) {
        return 1 / sqrt(a * pow(x, 2) + b * x + c);
    }

    public static double taskR(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    public static double taskS(double x) {
        return abs(x) + abs(x + 1);
    }
}