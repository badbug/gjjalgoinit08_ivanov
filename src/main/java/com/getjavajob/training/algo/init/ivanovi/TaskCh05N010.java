package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double rate = scanner.nextDouble();
        double[] exchangeTable = new double[20];
        exchangeTable = converter(exchangeTable, rate);
        arrayPrint(exchangeTable);
    }

    public static double[] converter(double[] exchangeTable, double rate) {
        for (int i = 0; i < exchangeTable.length; i++) {
            exchangeTable[i] = (i + 1) * rate;
            int tempInt = (int) (exchangeTable[i] * 100); //round to 2 digits after comma
            exchangeTable[i] = tempInt / 100.0;           //round to 2 digits after comma
        }
        return exchangeTable;
    }

    public static void arrayPrint(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Exchange rate for " + (i + 1) + " USD = " + array[i] + " RUR");
        }
    }
}