package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.PrintArray.*;

public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] array = {2, -4, -2, 1, 6, -7, 2};
        int[] newArray = new int[array.length];
        newArray = firstPositiveThenNegative(array, newArray);
        System.out.println(printArray(newArray));
    }

    public static int[] firstPositiveThenNegative(int[] array, int[] newArray) {
        int index = 0;
        int fromStartIndex = 0;
        int fromEndIndex = array.length - 1;
        while (index < array.length) {
            if (array[index] < 0) {
                newArray[fromStartIndex] = array[index];
                fromStartIndex++;
                index++;
            } else {
                newArray[fromEndIndex] = array[index];
                fromEndIndex--;
                index++;
            }
        }
        return newArray;
    }
}