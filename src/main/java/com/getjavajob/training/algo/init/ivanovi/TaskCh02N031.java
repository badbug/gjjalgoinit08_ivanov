package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input number from 100 to 999");
        int number = scanner.nextInt();
        if (number >= 100 && number <= 999) {
            int finalNumber = reverseSecondAndThirdChar(number);
            System.out.println(finalNumber);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static int reverseSecondAndThirdChar(int number) {
        int lastDigit = number % 10;
        int middleDigit = number / 10 % 10;
        int firstDigit = number / 100;
        return firstDigit * 100 + lastDigit * 10 + middleDigit;
    }
}