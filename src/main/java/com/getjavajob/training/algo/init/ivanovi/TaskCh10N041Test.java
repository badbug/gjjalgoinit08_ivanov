package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N041.*;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        factorialTest();
    }

    public static void factorialTest() {
        assertEquals("TaskCh10N041Test.factorialTest", 120, factorial(5));
    }
}