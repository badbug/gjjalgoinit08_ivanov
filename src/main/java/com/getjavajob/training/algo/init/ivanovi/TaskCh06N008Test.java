package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh06N008.*;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        checkMaxValueInArrayTest();
    }

    public static void checkMaxValueInArrayTest() {
        int[] expected = {1, 4, 9};
        assertEquals("TaskCh06N008Test.checkMaxValueInArrayTest", expected, checkMaxValueInArray(10));
    }
}