package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitsSumInNumber = digitsSumFunc(number);
        int digitsCountInNumber = digitsCountFunc(number);
        System.out.println("digits sum = " + digitsSumInNumber);
        System.out.println("digits count = " + digitsCountInNumber);
    }

    public static int digitsSumFunc(int n) {
        if (n == 0) {
            return 0;
        }
        return digitsSumFunc(n / 10) + n % 10;
    }

    public static int digitsCountFunc(int n) {
        if (n == 0) {
            return 0;
        }
        return digitsCountFunc(n / 10) + 1;
    }
}