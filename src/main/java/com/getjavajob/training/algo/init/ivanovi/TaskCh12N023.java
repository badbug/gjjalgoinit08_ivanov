package com.getjavajob.training.algo.init.ivanovi;

import java.util.Arrays;

public class TaskCh12N023 {
    public static final int ROWS = 6;
    public static final int COLS = 6;

    public static void main(String[] args) {
        int[][] array = new int[ROWS][COLS];
        int[][] arrayTaskA = fillArrayTaskA(array);
        int[][] arrayTaskB = fillArrayTaskB(array);
        int[][] arrayTaskC = fillArrayTaskC(array);
        System.out.println(Arrays.deepToString(arrayTaskA));
        System.out.println(Arrays.deepToString(arrayTaskB));
        System.out.println(Arrays.deepToString(arrayTaskC));
    }

    public static int[][] fillArrayTaskA(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            array[i][i] = 1;
        }
        for (int j = 0; j < array[1].length; j++) {
            array[j][array[1].length - 1 - j] = 1;
        }
        return array;
    }

    public static int[][] fillArrayTaskB(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            array[i][i] = 1;
            array[i][array[0].length / 2] = 1;
        }
        for (int j = 0; j < array[1].length; j++) {
            array[j][array[1].length - 1 - j] = 1;
            array[array[1].length / 2][j] = 1;
        }
        return array;
    }

    public static int[][] fillArrayTaskC(int[][] array) {
        int counter = 0;
        for (int i = 0; i < array[0].length; i++, counter++) {
            for (int j = 0; j < array[1].length; j++) {
                if (j >= counter && j <= array[1].length - 1 - counter) {
                    array[i][j] = 1;
                }
            }
        }
        counter = 0;
        for (int i = array[0].length - 1; i > (array[0].length - 1) / 2; i--, counter++) {
            for (int j = 0; j < array[1].length; j++) {
                if (j >= counter && j <= array[1].length - 1 - counter) {
                    array[i][j] = 1;
                }
            }
        }
        return array;
    }
}