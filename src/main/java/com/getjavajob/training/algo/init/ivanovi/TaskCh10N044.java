package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N044 {
    private static int sum = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitalSqrt = digitalSqrtFunc(number);
        System.out.println(digitalSqrt);
    }

    public static int digitalSqrtFunc(int n) {
        int tmpSum = 0;
        if (n == 0) {
            if (sum >= 10) {
                tmpSum = sum;
                sum = 0;
                digitalSqrtFunc(tmpSum);
            } else {
                return sum;
            }
        }
        if (n < 10) {
            sum += n;
        } else {
            sum += n % 10;
        }
        return digitalSqrtFunc(n / 10);
    }
}