package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh05N010.*;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        converterTester();
    }

    public static void converterTester() {
        double[] tableInit = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        double rate = 30.0;
        double[] expectedTable = {30.0, 60.0, 90.0, 120.0, 150.0, 180.0, 210.0, 240.0, 270.0, 300.0, 330.0, 360.0, 390.0, 420.0, 450.0, 480.0, 510.0, 540.0, 570.0, 600.0};
        assertEquals("TaskCh05N010Test.converterTester", expectedTable, converter(tableInit, rate));
    }
}