package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh02N039.*;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        calcAngleTest();
    }

    public static void calcAngleTest() {
        assertEquals("TaskCh02N039Test.calcAngleTest", 45, calcAngle(1, 30, 0));
    }
}
