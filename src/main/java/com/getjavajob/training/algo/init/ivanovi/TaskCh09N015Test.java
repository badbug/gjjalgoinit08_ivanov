package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N015.*;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        getCharAtNPositionTest();
    }

    public static void getCharAtNPositionTest() {
        assertEquals("TaskCh09N015Test.getCharAtNPositionTest", 'H', getCharAtNPosition("Hello", 1));
    }
}