package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter minute from 0 to 60 to check what is color on traffic light: ");
        int minute = scanner.nextInt();
        if (minute >= 0 && minute <= 60) {
            String color = checkTrafficLightColor(minute);
            System.out.println(color);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static String checkTrafficLightColor(int minute) {
        return minute % 3 < 2 ? "red" : "green";
    }
}