package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        if (inputString.length() % 2 == 0) {
            String halfOfWord = getHalfOfWord(inputString);
            System.out.println(halfOfWord);
        } else
            System.out.println("Incorrect length of word: " + inputString.length() + ". Expected even.");
    }

    public static String getHalfOfWord(String inputString) {
        return inputString.substring(0, inputString.length() / 2);
    }
}