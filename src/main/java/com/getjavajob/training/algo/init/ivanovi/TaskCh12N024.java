package com.getjavajob.training.algo.init.ivanovi;

import java.util.Arrays;

public class TaskCh12N024 {
    public static final int ROWS = 6;
    public static final int COLS = 6;

    public static void main(String[] args) {
        int[][] array = new int[ROWS][COLS];
        int[][] arrayTaskA = fillArrayTaskA(array);
        int[][] arrayTaskB = fillArrayTaskB(array);
        System.out.println(Arrays.deepToString(arrayTaskA));
        System.out.println(Arrays.deepToString(arrayTaskB));
    }

    public static int[][] fillArrayTaskA(int[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0) {
                    array[i][j] = 1;
                } else if (j == 0) {
                    array[i][j] = 1;
                } else {
                    array[i][j] = array[i][j - 1] + array[i - 1][j];
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskB(int[][] array) {
        int endNumberToFill = array[1].length - 1;
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i + j > endNumberToFill) {
                    array[i][j] = i + j - endNumberToFill;
                } else {
                    array[i][j] = i + j + 1;
                }
            }
        }
        return array;
    }
}