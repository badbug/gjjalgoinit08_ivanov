package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh04N015.*;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        checkHowOld1();
        checkHowOld2();
        checkHowOld3();
    }

    public static void checkHowOld1() {
        assertEquals("TaskCh04N015Test.checkHowOld1", 29, checkHowOld(12, 2014, 6, 1985));
    }

    public static void checkHowOld2() {
        assertEquals("TaskCh04N015Test.checkHowOld2", 28, checkHowOld(5, 2014, 6, 1985));
    }

    public static void checkHowOld3() {
        assertEquals("TaskCh04N015Test.checkHowOld3", 29, checkHowOld(6, 2014, 6, 1985));
    }
}