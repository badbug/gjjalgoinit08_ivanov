package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh02N043.*;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        checkDivisionTest();
    }

    public static void checkDivisionTest() {
        assertEquals("TaskCh02N043Test.checkDivisionTest", 1, checkDivision(3, 6));
    }
}
