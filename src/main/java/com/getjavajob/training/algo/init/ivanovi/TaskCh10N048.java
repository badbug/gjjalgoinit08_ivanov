package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh10N048 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 11, 112, 67, 54, 2};
        int counter = array.length;
        int maximum = array[0];
        int maxElementInArray = findMaxElement(counter, array, maximum);
        System.out.println(maxElementInArray);
    }

    public static int findMaxElement(int counter, int[] array, int maximum) {
        if (counter == 0) {
            return maximum;
        }
        if (array[counter - 1] > maximum) {
            maximum = array[counter - 1];
        }
        return findMaxElement(counter - 1, array, maximum);
    }
}