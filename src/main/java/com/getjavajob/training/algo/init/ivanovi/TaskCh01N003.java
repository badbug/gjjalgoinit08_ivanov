package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Your number " + n);
    }
}