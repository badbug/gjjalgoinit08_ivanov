package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh10N051 {
    public static void main(String[] args) {
        int n = 5;
        procedure1(n);
        procedure2(n);
        procedure3(n);
    }

    public static void procedure1(int n) {
        if (n > 0) {
            System.out.println(n);
            procedure1(n - 1);
        }
    }

    public static void procedure2(int n) {
        if (n > 0) {
            procedure2(n - 1);
            System.out.println(n);
        }
    }

    public static void procedure3(int n) {
        if (n > 0) {
            System.out.println(n);
            procedure3(n - 1);
            System.out.println(n);
        }
    }
}