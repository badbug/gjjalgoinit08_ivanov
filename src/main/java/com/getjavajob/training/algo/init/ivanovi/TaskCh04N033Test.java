package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh04N033.*;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        isEvenTest();
        isOddTest();
    }

    public static void isEvenTest() {
        assertEquals("TaskCh04N033Test.isEvenTest", true, checkLastDigitIsEven(1234778));
    }

    public static void isOddTest() {
        assertEquals("TaskCh04N033Test.isOddTest", true, checkLastDigitIsOdd(12345));
    }
}