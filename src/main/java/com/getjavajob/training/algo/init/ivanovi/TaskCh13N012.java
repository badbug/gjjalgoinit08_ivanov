package com.getjavajob.training.algo.init.ivanovi;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskCh13N012 {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Igor", "Vladimirovich", "Ivanov", "Podolsk, Filippova 3 -180", 4, 2012));
        employees.add(new Employee("Ivan", "Leonidovich", "Pavlushin", "Podolsk, Filippova 7-121", 6, 2013));
        employees.add(new Employee("Igor", "Gennadievich", "Magafurov", "Reutov, ul. Lenina, 1-1", 2, 2011));
        employees.add(new Employee("Dmitry", "Georgievich", "Chkoniya", "Moscow, Krasnaya str, 1-1", 7, 2014));
        employees.add(new Employee("Dmitry", "Sergeevich", "Popov", "Moscow, Krasnaya str, 1-1", 6, 2015));
        employees.add(new Employee("Sergey", "Leonidovich", "Evseev", "Podolsk, Filippova 7-121", 2, 2012));
        employees.add(new Employee("Galina", "Timofeevna", "Khvan", "Moscow, Krasnaya str, 1-1", 3, 2014));
        employees.add(new Employee("Artem", "Vladimirovich", "Aronchikov", "Podolsk, Filippova 7 -121", 1, 2016));
        employees.add(new Employee("Alexey", "Alexeevich", "Zhukov", "Moscow, Krasnaya str, 1-1", 8, 2013));
        employees.add(new Employee("Andrey", "Vladimirovich", "Polubotko", "Moscow, Krasnaya str, 1-1", 1, 2015));
        employees.add(new Employee("Alexey", "Sergeevich", "Sokolov", "Podolsk, Filippova 7-122", 9, 2013));
        employees.add(new Employee("Rashat", "Abdillin", "Podolsk, Filippova 7-123", 10, 2013));
        employees.add(new Employee("Anton", "Ivanovich", "Smelyanskiy", "Podolsk, Filippova 7-124", 2, 2014));
        employees.add(new Employee("Sergey", "Vladimirovich", "Panferov", "Podolsk, Filippova 7-125", 11, 2014));
        employees.add(new Employee("Nikolay", "Dmitrievich", "Kovtun", "Podolsk, Filippova 7-126", 12, 2010));
        employees.add(new Employee("Vladimir", "Andreevich", "Nosikov", "Podolsk, Filippova 7-127", 3, 2013));
        employees.add(new Employee("Alexandr", "Alexandrovich", "Zhukov", "Podolsk, Filippova 7-129", 7, 2013));
        employees.add(new Employee("Dmitry", "Sergeevich", "Nochevnov", "Podolsk, Filippova 7-130", 3, 2014));
        employees.add(new Employee("Mikhail", "Valentinovich", "Fursikov", "Podolsk, Filippova 17-1", 6, 2015));
        employees.add(new Employee("Denis", "Dmitrievich", "Snegirev", "Podolsk, Filippova 19-2", 10, 2014));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Searching employee by substring. Enter substring: ");
        String substring = scanner.nextLine();
        Database database = new Database(employees);
        System.out.println("Founded employees by substring \"" + substring + "\":");
        database.searchEmployee(substring);
        printList(database.searchEmployee(substring));

        System.out.print("Searching employees by experience. Enter number of years: ");
        int years = scanner.nextInt();
        System.out.print("Enter current month number: ");
        int monthNow = scanner.nextInt();
        System.out.print("Enter current year number: ");
        int yearNow = scanner.nextInt();
        System.out.println("\nEmployees with more than " + years + " experience: ");
        printList(database.getEmployeeWithMoreThanNYearExperience(years, monthNow, yearNow));

        System.out.println();
        System.out.println("Printing employees with their experience in years: ");
        Employee employeesExperience = new Employee();
        printIntegerList(employeesExperience.getEmployeeExperience(employees, monthNow, yearNow));
    }

    public static void printList(List<Employee> data) {
        for (Employee object : data) {
            System.out.println(object.getFirstName() + " " + object.getMiddleName() + " " + object.getLastName() + ", " + object.getAddress() + ", " + object.getMonth() + ", " + object.getYear());
        }
    }

    public static void printIntegerList(List<Integer> data) {
        for (Integer object : data) {
            System.out.println(object.toString());
        }
    }
}

class Employee {
    private String firstName;
    private String middleName = "";
    private String lastName;
    private String address;
    private int month;
    private int year;

    public Employee(String firstName, String lastName, String address, int month, int year) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.month = month;
        this.year = year;
    }

    public Employee(String firstName, String middleName, String lastName, String address, int month, int year) {
        this(firstName, lastName, address, month, year);
        this.middleName = middleName;
    }

    public Employee() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Integer> getEmployeeExperience(List<Employee> employees, int monthNow, int yearNow) {
        List<Integer> experience = new ArrayList<>();
        for (Employee object : employees) {
            int fullExperienceInYears = yearNow - object.getYear();
            if (monthNow <= object.getMonth()) {
                fullExperienceInYears -= 1;
            }
            experience.add(fullExperienceInYears);
        }
        return experience;
    }
}

class Database {
    private List<Employee> employees = new ArrayList<>();

    public Database(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> searchEmployee(String substring) {
        List<Employee> foundedEmployeesBySubstring = new ArrayList<>();
        for (Employee object : employees) {
            String caseInsensitiveFirstName = object.getFirstName().toLowerCase();
            String caseInsensitiveMiddleName = object.getMiddleName().toLowerCase();
            String caseInsensitiveLastName = object.getLastName().toLowerCase();
            substring = substring.toLowerCase();
            if (caseInsensitiveFirstName.contains(substring) || caseInsensitiveMiddleName.contains(substring) || caseInsensitiveLastName.contains(substring)) {
                foundedEmployeesBySubstring.add(object);
            }
        }
        return foundedEmployeesBySubstring;
    }

    public List<Employee> getEmployeeWithMoreThanNYearExperience(int n, int monthNow, int yearNow) {
        List<Employee> employeeExperience = new ArrayList<>();
        for (Employee object : employees) {
            int fullExperienceInYears = yearNow - object.getYear();
            if (monthNow <= object.getMonth()) {
                fullExperienceInYears -= 1;
            }
            if (fullExperienceInYears >= n) {
                employeeExperience.add(object);
            }
        }
        return employeeExperience;
    }
}