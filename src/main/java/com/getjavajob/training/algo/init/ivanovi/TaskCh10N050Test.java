package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N050.*;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        ackermannFuncTest();
    }

    public static void ackermannFuncTest() {
        assertEquals("TaskCh10N050Test.ackermannFuncTest", 61, ackermannFunc(3, 3));
    }
}