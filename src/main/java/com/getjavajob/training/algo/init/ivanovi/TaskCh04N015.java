package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input employee birthday month number");
        int birthDayMonth = scanner.nextInt();
        System.out.println("Input employee birthday year");
        int birthDayYear = scanner.nextInt();
        System.out.println("Input today month number");
        int todayMonth = scanner.nextInt();
        System.out.println("Input today year");
        int todayYear = scanner.nextInt();
        int age = checkHowOld(todayMonth, todayYear, birthDayMonth, birthDayYear);
        System.out.println("Employee age: " + age);
    }

    public static int checkHowOld(int todayMonth, int todayYear, int birthDayMonth, int birthDayYear) {
        int age = todayYear - birthDayYear;
        if (todayMonth < birthDayMonth) {
            age--;
        }
        return age;
    }
}