package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        boolean checkFirstLastChars = isFirstLastCharsEquals(inputString);
        System.out.println(checkFirstLastChars);
    }

    public static boolean isFirstLastCharsEquals(String inputString) {
        return inputString.charAt(0) == inputString.charAt(inputString.length() - 1);
    }
}