package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter value to be translated: ");
        int number = scanner.nextInt();
        System.out.print("Enter numeral system from 2 to 16: ");
        int base = scanner.nextInt();
        String translatedValue = fromDecimalToAnotherBase(number, base, 0, "");
        System.out.println(translatedValue);
    }

    public static String fromDecimalToAnotherBase(int number, int base, int position, String result) {
        char chars[] = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int int1 = (int) Math.pow(base, position);
        int int2 = (int) Math.pow(base, position + 1);
        if (number < int2) {
            return chars[number / int2] + result;
        } else {
            int remainder = number % int2;
            return fromDecimalToAnotherBase(number - remainder, base, position + 1, chars[remainder / int1] + result);
        }
    }
}