package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh05N064.*;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        countAvgDensityTest();
    }

    public static void countAvgDensityTest() {
        double[] peopleCount = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        double[] area = {12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh05N064Test.countAvgDensityTest", 1.0, countAvgDensity(peopleCount, area));
    }
}