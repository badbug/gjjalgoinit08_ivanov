package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int isDivideAonBorBonA = checkDivision(a, b);
        System.out.println(isDivideAonBorBonA);
    }

    public static int checkDivision(int a, int b) {
        return a % b == 0 || b % a == 0 ? 1 : 0;
    }
}