package com.getjavajob.training.algo.init.ivanovi;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh13N012.*;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Igor", "Vladimirovich", "Ivanov", "Podolsk, Filippova 3 -180", 4, 2012));
        employees.add(new Employee("Ivan", "Leonidovich", "Pavlushin", "Podolsk, Filippova 7-121", 6, 2013));
        employees.add(new Employee("Igor", "Gennadievich", "Magafurov", "Reutov, ul. Lenina, 1-1", 2, 2011));
        employees.add(new Employee("Dmitry", "Georgievich", "Chkoniya", "Moscow, Krasnaya str, 1-1", 7, 2014));
        employees.add(new Employee("Dmitry", "Sergeevich", "Popov", "Moscow, Krasnaya str, 1-1", 6, 2015));
        employees.add(new Employee("Sergey", "Leonidovich", "Evseev", "Podolsk, Filippova 7-121", 2, 2012));
        employees.add(new Employee("Galina", "Timofeevna", "Khvan", "Moscow, Krasnaya str, 1-1", 3, 2014));
        employees.add(new Employee("Artem", "Vladimirovich", "Aronchikov", "Podolsk, Filippova 7 -121", 1, 2016));
        employees.add(new Employee("Alexey", "Alexeevich", "Zhukov", "Moscow, Krasnaya str, 1-1", 8, 2013));
        employees.add(new Employee("Andrey", "Vladimirovich", "Polubotko", "Moscow, Krasnaya str, 1-1", 1, 2015));
        employees.add(new Employee("Alexey", "Sergeevich", "Sokolov", "Podolsk, Filippova 7-122", 9, 2013));
        employees.add(new Employee("Rashat", "Abdillin", "Podolsk, Filippova 7-123", 10, 2013));
        employees.add(new Employee("Anton", "Ivanovich", "Smelyanskiy", "Podolsk, Filippova 7-124", 2, 2014));
        employees.add(new Employee("Sergey", "Vladimirovich", "Panferov", "Podolsk, Filippova 7-125", 11, 2014));
        employees.add(new Employee("Nikolay", "Dmitrievich", "Kovtun", "Podolsk, Filippova 7-126", 12, 2010));
        employees.add(new Employee("Vladimir", "Andreevich", "Nosikov", "Podolsk, Filippova 7-127", 3, 2013));
        employees.add(new Employee("Alexandr", "Alexandrovich", "Zhukov", "Podolsk, Filippova 7-129", 7, 2013));
        employees.add(new Employee("Dmitry", "Sergeevich", "Nochevnov", "Podolsk, Filippova 7-130", 3, 2014));
        employees.add(new Employee("Mikhail", "Valentinovich", "Fursikov", "Podolsk, Filippova 17-1", 6, 2015));
        employees.add(new Employee("Denis", "Dmitrievich", "Snegirev", "Podolsk, Filippova 19-2", 10, 2014));
        searchEmployeeTest(employees, "den");
        getEmployeeExperienceTest(employees, 7, 2016);
        getEmployeeWithMoreThanNYearExperienceTest(employees, 5, 7, 2016);
    }

    public static void searchEmployeeTest(List<Employee> employeeList, String substring) {
        List<Employee> expected = new ArrayList<>();
        expected.add(employeeList.get(19));
        Database database = new Database(employeeList);
        assertEquals("TaskCh13N012Test.searchEmployeeTest", expected, database.searchEmployee(substring));
    }

    public static void getEmployeeExperienceTest(List<Employee> employees, int monthNow, int yearNow) {
        List<Integer> expected = new ArrayList<>();
        expected.add(4);
        expected.add(3);
        expected.add(5);
        expected.add(1);
        expected.add(1);
        expected.add(4);
        expected.add(2);
        expected.add(0);
        expected.add(2);
        expected.add(1);
        expected.add(2);
        expected.add(2);
        expected.add(2);
        expected.add(1);
        expected.add(5);
        expected.add(3);
        expected.add(2);
        expected.add(2);
        expected.add(1);
        expected.add(1);
        Employee employee = new Employee();
        assertEquals("TaskCh13N012Test.getEmployeeExperienceTest", expected, employee.getEmployeeExperience(employees, monthNow, yearNow));
    }

    public static void getEmployeeWithMoreThanNYearExperienceTest(List<Employee> employees, int years, int monthNow, int yearNow) {
        List<Employee> expected = new ArrayList<>();
        expected.add(employees.get(2));
        expected.add(employees.get(14));
        Database database = new Database(employees);
        assertEquals("TaskCh13N012Test.getEmployeeWithMoreThanNYearExperienceTest", expected, database.getEmployeeWithMoreThanNYearExperience(years, monthNow, yearNow));
    }
}