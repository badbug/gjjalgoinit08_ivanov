package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int hours = scanner.nextInt();
        int minutes = scanner.nextInt();
        int seconds = scanner.nextInt();
        if (hours > 0 && hours <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds < 59) {
            double angle = calcAngle(hours, minutes, seconds);
            System.out.println(angle);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static double calcAngle(int hours, int minutes, int seconds) {
        if (hours > 12) {
            hours -= 12;
        }
        double degreeInHours = hours * 360D / 12;
        double degreeInMinutes = minutes * 360D / 12 / 60;
        double degreeInSeconds = seconds * 360D / 12 / 60 / 60;
        return degreeInHours + degreeInMinutes + degreeInSeconds;
    }
}