package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N107.*;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        reversCharsTest();
    }

    public static void reversCharsTest() {
        assertEquals("TaskCh09N107Test.reversCharsTest", "sorako", reversChars("soroka"));
    }
}