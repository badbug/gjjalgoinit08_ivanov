package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N052.*;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        reverseIntFuncTest();
    }

    public static void reverseIntFuncTest() {
        assertEquals("TaskCh10N052Test.reverseIntFuncTest", 654, reverseIntFunc(456, 0));
    }
}