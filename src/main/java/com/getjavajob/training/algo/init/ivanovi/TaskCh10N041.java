package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int fact = factorial(number);
        System.out.println(fact);
    }

    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return factorial(n - 1) * n;
    }
}