package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N053.*;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        reverseArrayFuncTest();
    }

    public static void reverseArrayFuncTest() {
        int[] notReversedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] reversedArray = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh10N053Test.reverseArrayFuncTest", reversedArray, reverseArrayFunc(notReversedArray, 0));
    }
}