package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh02N013.*;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        reverseNumberTest();
    }

    public static void reverseNumberTest() {
        assertEquals("TaskCh02N013Test.reverseNumberTest", 321, reverseNumber(123));
    }
}
