package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N042.*;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        powFuncTest();
    }

    public static void powFuncTest() {
        assertEquals("TaskCh10N042Test.powFuncTest", 1024, powFunc(2, 10));
    }
}