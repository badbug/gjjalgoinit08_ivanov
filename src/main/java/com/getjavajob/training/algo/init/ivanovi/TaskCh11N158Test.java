package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh11N158.*;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        deleteDuplicatesInArrayTest();
    }

    public static void deleteDuplicatesInArrayTest() {
        int[] array = {1, 1, 2, 3, 4, 4, 5, 6, 6, 6, 7, 7, 7, 7, 7, 8, 9, 9, 9, 9};
        int[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        assertEquals("TaskCh11N158Test.deleteDuplicatesInArrayTest", expectedArray, deleteDuplicatesInArray(array));
    }
}