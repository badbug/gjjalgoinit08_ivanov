package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N048.*;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        int[] array = {1, 334, 67, 98, 123, 777, 666, 7868};
        int arraySize = 8;
        int maximum = 0;
        findMaxElementTest(arraySize, array, maximum);
    }

    public static void findMaxElementTest(int n, int[] array, int maximum) {
        assertEquals("TaskCh10N048Test.findMaxElementTest", 7868, findMaxElement(n, array, maximum));
    }
}