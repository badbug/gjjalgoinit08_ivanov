package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        String reverseAandO = reversChars(inputString);
        System.out.println(reverseAandO);
    }

    public static String reversChars(String inputString) {
        int aPosition = inputString.indexOf('a');
        int oPosition = inputString.lastIndexOf('o');
        StringBuilder newString = new StringBuilder(inputString);
        if (aPosition == -1 || oPosition == -1) {
            return "Error, this word doesn't have 'a' or 'o' chars";
        } else {
            newString.setCharAt(aPosition, 'o');
        }
        newString.setCharAt(oPosition, 'a');
        return newString.toString();
    }
}