package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N043.*;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        digitsSumFuncTest();
        digitsCountFuncTest();
    }

    public static void digitsSumFuncTest() {
        assertEquals("TaskCh10N043Test.digitsSumFuncTest", 15, digitsSumFunc(12345));
    }

    public static void digitsCountFuncTest() {
        assertEquals("TaskCh10N043Test.digitsCountFuncTest", 5, digitsCountFunc(12345));
    }
}