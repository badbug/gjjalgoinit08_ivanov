package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N056.*;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        isSimpleCheckTest();
    }

    public static void isSimpleCheckTest() {
        assertEquals("TaskCh10N056Test.isSimpleCheckTest", true, isSimpleCheck(103, 2));
    }
}