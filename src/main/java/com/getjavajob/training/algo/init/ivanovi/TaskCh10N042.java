package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int power = scanner.nextInt();
        int number = scanner.nextInt();
        int pow = powFunc(power, number);
        System.out.println(pow);
    }

    public static int powFunc(int power, int n) {
        if (n == 0) {
            return 1;
        }
        return powFunc(power, n - 1) * power;
    }
}