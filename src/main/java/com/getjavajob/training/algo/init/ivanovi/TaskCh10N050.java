package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh10N050 {
    public static void main(String[] args) {
        int n = 1;
        int m = 3;
        int ackermannInt = ackermannFunc(n, m);
        System.out.println(ackermannInt);
    }

    public static int ackermannFunc(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return ackermannFunc(n - 1, 1);
        } else {
            return ackermannFunc(n - 1, ackermannFunc(n, m - 1));
        }
    }
}