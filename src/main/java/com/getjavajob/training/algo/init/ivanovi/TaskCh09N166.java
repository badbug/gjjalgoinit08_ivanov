package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        String newStr = reverseFirstAndLastWords(inputString);
        System.out.println(newStr);
    }

    public static String reverseFirstAndLastWords(String inputString) {
        String[] newString = inputString.split(" ");
        String reverseStr = "";
        if (newString.length > 2) {
            String tmpStr = "";
            for (int i = 1; i < newString.length - 1; i++) {
                tmpStr += newString[i];
            }
            reverseStr = newString[newString.length - 1] + " " + tmpStr + " " + newString[0];
        } else if (newString.length == 2) {
            reverseStr = newString[1] + " " + newString[0];
        } else {
            reverseStr = "Sentence must contains at least 2 words";
        }
        return reverseStr;
    }
}