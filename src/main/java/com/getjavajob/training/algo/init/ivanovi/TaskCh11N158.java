package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.PrintArray.printArray;

public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 3};
        int[] arrayWithUniqueValues = deleteDuplicatesInArray(array);
        System.out.println(printArray(arrayWithUniqueValues));
    }

    public static int[] deleteDuplicatesInArray(int[] array) {
        int index1 = 0;
        int index2 = 0;
        int shiftCounter = 0;
        while (index1 < array.length - 1 - shiftCounter) {
            if (index2 == array.length - 1) {
                index1++;
                index2 = index1;
            } else if (array[index1] == array[index2 + 1]) {
                shiftCounter++;
                shiftElementsInArray(array, index2 + 1, shiftCounter);
            } else {
                index2++;
            }
        }
        return array;
    }

    public static void shiftElementsInArray(int[] array, int index, int shiftCounter) {
        while (index < array.length - 1) {
            array[index] = array[index + 1];
            index++;
        }
        array[array.length - shiftCounter] = 0;
    }
}