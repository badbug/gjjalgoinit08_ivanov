package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh05N038.*;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        calcDistanceTest();
    }

    public static void calcDistanceTest() {
        double[] expected = {2.93, 0.65};
        assertEquals("TaskCh05N038Test.calcDistanceTest", expected, calcDistance(10));
    }
}