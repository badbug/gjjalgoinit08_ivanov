package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input number to reverse from 100 to 200: ");
        int number = scanner.nextInt();
        if (number >= 100 && number <= 200) {
            int reversedNumber = reverseNumber(number);
            System.out.println(reversedNumber);
        } else {
            System.out.println("Input out of range");
        }
    }

    public static int reverseNumber(int number) {
        int lastDigit = number % 10;
        int middleDigit = number / 10 % 10;
        int firstDigit = number / 100;
        return lastDigit * 100 + middleDigit * 10 + firstDigit;
    }
}