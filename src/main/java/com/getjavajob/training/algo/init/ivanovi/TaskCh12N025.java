package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh12N025 {
    public static final int ROWS = 12;
    public static final int COLS = 10;

    public static void main(String[] args) {
        int[][] arrayA = new int[ROWS][COLS];
        int[][] arrayB = new int[ROWS][COLS];
        int[][] arrayC = new int[ROWS][COLS];
        int[][] arrayD = new int[ROWS][COLS];
        int[][] arrayE = new int[10][12];
        int[][] arrayF = new int[ROWS][COLS];
        int[][] arrayG = new int[ROWS][COLS];
        int[][] arrayH = new int[ROWS][COLS];
        int[][] arrayI = new int[ROWS][COLS];
        int[][] arrayJ = new int[ROWS][COLS];
        int[][] arrayK = new int[ROWS][COLS];
        int[][] arrayL = new int[ROWS][COLS];
        int[][] arrayM = new int[ROWS][COLS];
        int[][] arrayN = new int[ROWS][COLS];
        int[][] arrayO = new int[ROWS][COLS];
        int[][] arrayP = new int[ROWS][COLS];
        int[][] arrayTaskA = fillArrayTaskA(arrayA);
        int[][] arrayTaskB = fillArrayTaskB(arrayB);
        int[][] arrayTaskC = fillArrayTaskC(arrayC);
        int[][] arrayTaskD = fillArrayTaskD(arrayD);
        int[][] arrayTaskE = fillArrayTaskE(arrayE);
        int[][] arrayTaskF = fillArrayTaskF(arrayF);
        int[][] arrayTaskG = fillArrayTaskG(arrayG);
        int[][] arrayTaskH = fillArrayTaskH(arrayH);
        int[][] arrayTaskI = fillArrayTaskI(arrayI);
        int[][] arrayTaskJ = fillArrayTaskJ(arrayJ);
        int[][] arrayTaskK = fillArrayTaskK(arrayK);
        int[][] arrayTaskL = fillArrayTaskL(arrayL);
        int[][] arrayTaskM = fillArrayTaskM(arrayM);
        int[][] arrayTaskN = fillArrayTaskN(arrayN);
        int[][] arrayTaskO = fillArrayTaskO(arrayO);
        int[][] arrayTaskP = fillArrayTaskP(arrayP);
        System.out.println("Task A");
        printMultyArray(arrayTaskA);
        System.out.println("Task B");
        printMultyArray(arrayTaskB);
        System.out.println("Task C");
        printMultyArray(arrayTaskC);
        System.out.println("Task D");
        printMultyArray(arrayTaskD);
        System.out.println("Task E");
        printMultyArray(arrayTaskE);
        System.out.println("Task F");
        printMultyArray(arrayTaskF);
        System.out.println("Task G");
        printMultyArray(arrayTaskG);
        System.out.println("Task H");
        printMultyArray(arrayTaskH);
        System.out.println("Task I");
        printMultyArray(arrayTaskI);
        System.out.println("Task J");
        printMultyArray(arrayTaskJ);
        System.out.println("Task K");
        printMultyArray(arrayTaskK);
        System.out.println("Task L");
        printMultyArray(arrayTaskL);
        System.out.println("Task M");
        printMultyArray(arrayTaskM);
        System.out.println("Task N");
        printMultyArray(arrayTaskN);
        System.out.println("Task O");
        printMultyArray(arrayTaskO);
        System.out.println("Task P");
        printMultyArray(arrayTaskP);
    }

    public static int[][] fillArrayTaskA(int[][] array) {
        int counter = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0 && j == 0) {
                    array[i][j] = counter++;
                } else if (j == 0 && i > 0) {
                    array[i][j] = counter++;
                } else {
                    array[i][j] = counter++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskB(int[][] array) {
        int counter = 1;
        for (int j = 0; j < array[1].length; j++) {
            for (int i = 0; i < array.length; i++) {
                if (i == 0 && j == 0) {
                    array[i][j] = counter++;
                } else if (i > 0) {
                    array[i][j] = counter++;
                } else if (i == 0 && j > 0) {
                    array[i][j] = counter++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskC(int[][] array) {
        int counter = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = array[1].length - 1; j >= 0; j--) {
                if (i == 0 && j == array[1].length - 1) {
                    array[i][j] = counter++;
                } else if (j >= 0 && j < array[1].length - 1) {
                    array[i][j] = counter++;
                } else if (j == array[1].length - 1) {
                    array[i][j] = counter++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskD(int[][] array) {
        int counter = 1;
        for (int j = 0; j < array[1].length; j++) {
            for (int i = array.length - 1; i >= 0; i--) {
                if (i == array.length - 1 && j == 0) {
                    array[i][j] = counter++;
                } else if (i >= 0 && i < array.length - 1) {
                    array[i][j] = counter++;
                } else if (i == array.length - 1) {
                    array[i][j] = counter++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskE(int[][] array) {
        int i = 0;
        int j = 0;
        int counter = 1;
        while (i != array.length) {
            if (i == 0 && j == 0) {
                array[i][j] = counter++;
                j++;
            }
            if (i % 2 == 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j++;
            } else if (i % 2 != 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (j == 0 && i > 0) {
                array[i][j] = counter++;
                i++;
                if (i < array.length - 1) {
                    array[i][j] = counter++;
                    j++;
                }
            }
            if (j == array[1].length - 1 && i >= 0) {
                array[i][j] = counter++;
                i++;
                array[i][j] = counter++;
                j--;
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskF(int[][] array) {
        int i = 0;
        int j = 0;
        int counter = 1;
        while (j != array[1].length) {
            if (i == 0 && j == 0) {
                array[i][j] = counter++;
                i++;
            }
            if (j % 2 == 0 && i > 0 && i < array.length - 1) {
                array[i][j] = counter++;
                i++;
            } else if (j % 2 != 0 && i > 0 && i < array.length - 1) {
                array[i][j] = counter++;
                i--;
            }
            if (i == 0 && j > 0) {
                array[i][j] = counter++;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter++;
                    i++;
                }
            }
            if (i == array.length - 1 && j >= 0) {
                array[i][j] = counter++;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter++;
                    i--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskG(int[][] array) {
        int i = array.length - 1;
        int j = 0;
        int counter = 1;
        while (i >= 0) {
            if (i == array.length - 1 && j == 0) {
                array[i][j] = counter++;
                j++;
            }
            if (j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j++;
            }
            if (j == array[1].length - 1) {
                array[i][j] = counter++;
                i--;
                j = 0;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskH(int[][] array) {
        int i = 0;
        int j = array[1].length - 1;
        int counter = 1;
        while (j >= 0) {
            if (i == 0 && j == array[1].length - 1) {
                array[i][j] = counter++;
                i++;
            }
            if (i > 0 && i < array.length - 1) {
                array[i][j] = counter++;
                i++;
            }
            if (i == array.length - 1) {
                array[i][j] = counter++;
                j--;
                i = 0;
                if (j >= 0) {
                    array[i][j] = counter++;
                    i++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskI(int[][] array) {
        int i = array.length - 1;
        int j = array[1].length - 1;
        int counter = 1;
        while (i >= 0) {
            if (i == array.length - 1 && j == array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (j == 0) {
                array[i][j] = counter++;
                i--;
                j = array[1].length - 1;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskJ(int[][] array) {
        int i = array.length - 1;
        int j = array[1].length - 1;
        int counter = 1;
        while (j >= 0) {
            if (i == array.length - 1 && j == array[1].length - 1) {
                array[i][j] = counter++;
                i--;
            }
            if (i > 0 && i < array.length - 1) {
                array[i][j] = counter++;
                i--;
            }
            if (i == 0) {
                array[i][j] = counter++;
                j--;
                i = array.length - 1;
                if (j >= 0) {
                    array[i][j] = counter++;
                    i--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskK(int[][] array) {
        int i = array.length - 1;
        int j = 0;
        int counter = 1;
        while (i >= 0) {
            if (i == array.length - 1 && j == 0) {
                array[i][j] = counter++;
                j++;
            }
            if (i % 2 != 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j++;
            } else if (i % 2 == 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (j == 0 && i >= 0) {
                array[i][j] = counter++;
                i--;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j++;
                }
            }
            if (j == array[1].length - 1 && i >= 0) {
                array[i][j] = counter++;
                i--;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskL(int[][] array) {
        int i = 0;
        int j = array[1].length - 1;
        int counter = 1;
        while (i < array.length) {
            if (i == 0 && j == array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (i % 2 == 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            } else if (i % 2 != 0 && j > 0 && j < array[1].length - 1) {
                array[i][j] = counter++;
                j++;
            }
            if (j == 0 && i >= 0) {
                array[i][j] = counter++;
                i++;
                if (i < array.length) {
                    array[i][j] = counter++;
                    j++;
                }
            }
            if (j == array[1].length - 1 && i >= 0) {
                array[i][j] = counter++;
                i++;
                if (i < array.length) {
                    array[i][j] = counter++;
                    j--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskM(int[][] array) {
        int i = 0;
        int j = 0;
        int counter = 120;
        while (j < array[1].length) {
            if (i == 0 && j == 0) {
                array[i][j] = counter--;
                i++;
            }
            if (i < array.length - 1 && j % 2 == 0) {
                array[i][j] = counter--;
                i++;
            } else if (i < array.length - 1 && j % 2 != 0) {
                array[i][j] = counter--;
                i--;
            }
            if (i == array.length - 1) {
                array[i][j] = counter--;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter--;
                    i--;
                }
            }
            if (i == 0 && j > 0) {
                array[i][j] = counter--;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter--;
                    i++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskN(int[][] array) {
        int i = array.length - 1;
        int j = 0;
        int counter = 1;
        while (j < array[1].length) {
            if (i == array.length - 1 && j == 0) {
                array[i][j] = counter++;
                i--;
            }
            if (i < array.length - 1 && j % 2 == 0) {
                array[i][j] = counter++;
                i--;
            } else if (i < array.length - 1 && j % 2 != 0) {
                array[i][j] = counter++;
                i++;
            }
            if (i == 0) {
                array[i][j] = counter++;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter++;
                    i++;
                }
            }
            if (i == array.length - 1) {
                array[i][j] = counter++;
                j++;
                if (j < array[1].length) {
                    array[i][j] = counter++;
                    i--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskO(int[][] array) {
        int i = array.length - 1;
        int j = array[1].length - 1;
        int counter = 1;
        while (i >= 0) {
            if (i == array.length - 1 && j == array[1].length - 1) {
                array[i][j] = counter++;
                j--;
            }
            if (j > 0 && i % 2 != 0) {
                array[i][j] = counter++;
                j--;
            } else if (j > 0 && i % 2 == 0) {
                array[i][j] = counter++;
                j++;
            }
            if (j == 0) {
                array[i][j] = counter++;
                i--;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j++;
                }
            }
            if (j == array[1].length - 1) {
                array[i][j] = counter++;
                i--;
                if (i >= 0) {
                    array[i][j] = counter++;
                    j--;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayTaskP(int[][] array) {
        int i = array.length - 1;
        int j = array[1].length - 1;
        int counter = 1;
        while (j >= 0) {
            if (i == array.length - 1 && j == array[1].length - 1) {
                array[i][j] = counter++;
                i--;
            }
            if (i > 0 && j % 2 != 0) {
                array[i][j] = counter++;
                i--;
            } else if (i > 0 && j % 2 == 0) {
                array[i][j] = counter++;
                i++;
            }
            if (i == 0) {
                array[i][j] = counter++;
                j--;
                if (j >= 0) {
                    array[i][j] = counter++;
                    i++;
                }
            }
            if (i == array.length - 1) {
                array[i][j] = counter++;
                j--;
                if (j >= 0) {
                    array[i][j] = counter++;
                    i--;
                }
            }
        }
        return array;
    }

    public static void printMultyArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
        System.out.println();
    }
}