package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N055.*;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        translateValueToOtherBaseTest();
    }

    public static void translateValueToOtherBaseTest() {
        assertEquals("TaskCh10N055Test.translateValueToOtherBaseTest", "A", fromDecimalToAnotherBase(10, 16, 0, ""));
    }
}
