package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int[] array = checkMaxValueInArray(number);
        printArray(array);
    }

    public static int[] checkMaxValueInArray(int number) {
        double maxDouble = Math.sqrt(number);
        int maxInt = (int) maxDouble;
        if (maxDouble == maxInt) {
            maxInt--;
        }
        int[] array = new int[maxInt];
        for (int i = 0; i < maxInt; i++) {
            array[i] = (i + 1) * (i + 1);
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}