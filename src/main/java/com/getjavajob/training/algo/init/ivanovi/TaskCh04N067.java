package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int dayNumber = scanner.nextInt();
        if (dayNumber <= 0) {
            System.out.println("Invalid day number");
        } else {
            String today = checkDay(dayNumber);
            System.out.println(today);
        }
    }

    public static String checkDay(int dayNumber) {
        return dayNumber % 7 == 6 || dayNumber % 7 == 0 ? "Weekend" : "Workday";
    }
}