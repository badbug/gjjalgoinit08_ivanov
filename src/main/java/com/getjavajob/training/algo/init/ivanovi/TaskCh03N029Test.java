package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh03N029.*;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        isAllOdd();
        isOneSmaller();
        isOneZeroEqual();
        isAllSubZero();
        isOnlyOneDivideByFive();
        isOneGreaterThanHundred();
    }

    public static void isAllOdd() {
        assertEquals("TaskCh03N029Test.isAllOdd", true, allOdd(3, 5));
    }

    public static void isOneSmaller() {
        assertEquals("TaskCh03N029Test.isOneSmaller", true, oneSmaller(10, 30));
    }

    public static void isOneZeroEqual() {
        assertEquals("TaskCh03N029Test.isOneZeroEqual", true, oneZeroEqual(0, 10));
    }

    public static void isAllSubZero() {
        assertEquals("TaskCh03N029Test.isAllSubZero", true, allSubZero(-5, -8, -20));
    }

    public static void isOnlyOneDivideByFive() {
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", true, onlyOneDivideByFive(1, 1, 5));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", false, onlyOneDivideByFive(1, 5, 5));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", false, onlyOneDivideByFive(5, 5, 5));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", false, onlyOneDivideByFive(1, 1, 1));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", false, onlyOneDivideByFive(5, 5, 1));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", true, onlyOneDivideByFive(5, 1, 1));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", true, onlyOneDivideByFive(1, 5, 1));
        assertEquals("TaskCh03N029Test.isOnlyOneDivideByFive", false, onlyOneDivideByFive(5, 1, 5));
    }

    public static void isOneGreaterThanHundred() {
        assertEquals("TaskCh03N029Test.isOneGreaterThanHundred", true, oneGreaterThanHundred(0, 7, 500));
    }
}