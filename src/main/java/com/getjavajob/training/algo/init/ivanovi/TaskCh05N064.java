package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] peopleCount = new double[12];
        double[] area = new double[12];
        for (int i = 0; i < 12; i++) {
            System.out.print("Enter count of people in area (in thousand people): ");
            peopleCount[i] = scanner.nextDouble();
            System.out.println("");
            System.out.print("Enter surface area: ");
            area[i] = scanner.nextDouble();
        }
        double avgDensity = countAvgDensity(peopleCount, area);
        System.out.println("Average density = " + avgDensity);
    }

    public static double countAvgDensity(double[] peopleCount, double[] area) {
        double peopleCounter = 0;
        double areaCounter = 0;
        for (int i = 0; i < 12; i++) {
            peopleCounter += peopleCount[i];
            areaCounter += area[i];
        }
        return peopleCounter / areaCounter;
    }
}