package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
        System.out.println(game.result());
    }
}

class Game {
    private int teamOneScore;
    private int teamTwoScore;
    private String teamOneName;
    private String teamTwoName;

    public Game() {
    }

    public Game(String teamOneName, String teamTwoName, int teamOneScore, int teamTwoScore) {
        this.teamOneName = teamOneName;
        this.teamTwoName = teamTwoName;
        this.teamOneScore = teamOneScore;
        this.teamTwoScore = teamTwoScore;
    }

    public void play() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter team #1: ");
        teamOneName = scanner.nextLine();
        System.out.print("Enter team #2: ");
        teamTwoName = scanner.nextLine();
        int teamNumber;
        while (true) {
            System.out.print("Enter team to score (1 or 2 or 0 to finish game): ");
            teamNumber = scanner.nextInt();
            int inputScore;
            if (teamNumber == 0) {
                break;
            } else if (teamNumber == 1) {
                System.out.println("Enter score (1 or 2 or 3): ");
                inputScore = scanner.nextInt();
                if (inputScore == 1 || inputScore == 2 || inputScore == 3) {
                    teamOneScore += inputScore;
                    System.out.println(intermediateResult());
                    continue;
                } else {
                    System.out.println("Input out of range");
                    break;
                }
            } else if (teamNumber == 2) {
                System.out.println("Enter score (1 or 2 or 3): ");
                inputScore = scanner.nextInt();
                if (inputScore == 1 || inputScore == 2 || inputScore == 3) {
                    teamTwoScore += inputScore;
                    System.out.println(intermediateResult());
                    continue;
                } else {
                    System.out.println("Input out of range");
                    break;
                }
            }
        }
    }

    public String result() {
        String result = "";
        if (teamOneScore > teamTwoScore) {
            result += teamOneName + ". ";
            result += "The final score is: " + teamOneScore + "-" + teamTwoScore;
        } else if (teamTwoScore > teamOneScore) {
            result += teamTwoName + ". ";
            result += "The final score is: " + teamTwoScore + "-" + teamOneScore;
        } else {
            result += "Draw. The final score is: ";
            result += teamTwoScore + "-" + teamOneScore;
        }
        return result;
    }

    public String intermediateResult() {
        return "Current score is: " + teamOneScore + "-" + teamTwoScore;
    }
}