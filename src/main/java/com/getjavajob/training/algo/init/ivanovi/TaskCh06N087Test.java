package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh06N087.*;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        resultWinTest();
        resultDrawTest();
        intermediateResultTest();
    }

    public static void resultWinTest() {
        Game resultWin = new Game("Team1", "Team2", 12, 2);
        assertEquals("TaskCh06N087Test.resultWinTest", "Team1. The final score is: 12-2", resultWin.result());
    }

    public static void resultDrawTest() {
        Game resultDraw = new Game("Team1", "Team2", 2, 2);
        assertEquals("TaskCh06N087Test.resultDrawTest", "Draw. The final score is: 2-2", resultDraw.result());
    }

    public static void intermediateResultTest() {
        Game result = new Game("Team1", "Team2", 2, 3);
        assertEquals("TaskCh06N087Test.intermediateResultTest", "Current score is: 2-3", result.intermediateResult());
    }
}