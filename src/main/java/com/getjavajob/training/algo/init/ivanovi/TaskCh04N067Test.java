package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh04N067.*;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        dayOfWeekTest1();
        dayOfWeekTest2();
    }

    public static void dayOfWeekTest1() {
        assertEquals("TaskCh04N067Test.dayOfWeekTest1", "Workday", checkDay(5));
    }

    public static void dayOfWeekTest2() {
        assertEquals("TaskCh04N067Test.dayOfWeekTest2", "Weekend", checkDay(28));
    }
}