package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh12N234.*;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        deleteRowInArrayTest();
        deleteColInArrayTest();
    }

    public static void deleteRowInArrayTest() {
        int[][] array = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5, 5},
                {6, 6, 6, 6, 6, 6}
        };
        int[][] expectedArray = new int[][]{
                {2, 2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5, 5},
                {6, 6, 6, 6, 6, 6},
                {0, 0, 0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.deleteRowInArrayTest", expectedArray, deleteRowInArray(array, 1));
    }

    public static void deleteColInArrayTest() {
        int[][] array = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6},
                {1, 2, 3, 4, 5, 6}
        };
        int[][] expectedArray = new int[][]{
                {2, 3, 4, 5, 6, 0},
                {2, 3, 4, 5, 6, 0},
                {2, 3, 4, 5, 6, 0},
                {2, 3, 4, 5, 6, 0},
                {2, 3, 4, 5, 6, 0},
                {2, 3, 4, 5, 6, 0}
        };
        assertEquals("TaskCh12N234Test.deleteColInArrayTest", expectedArray, deleteColInArray(array, 1));
    }
}