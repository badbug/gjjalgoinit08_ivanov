package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input array size, inputed value must be odd: ");
        int arraySize = scanner.nextInt();
        if (arraySize % 2 != 1)
            System.out.println("Invalid input: inputed value is not odd");
        else {
            int[][] array = new int[arraySize][arraySize];
            int[][] arrayTaskA = fillArray(array);
            printMultyArray(arrayTaskA);
        }
    }

    public static int[][] fillArray(int[][] array) {
        int stepsToBorder = array.length;
        int i = 0;
        int j = 0;
        int counter = 1;
        while (stepsToBorder > 0) {
            for (int y = 0; y < 4; y++) {
                for (int x = 0; x < stepsToBorder; x++) {
                    if (y == 0 && x < stepsToBorder - i) {
                        array[y + j][x + i] = counter;
                        counter++;
                    }
                    if (y == 1 && x < stepsToBorder - j && x != 0) {
                        array[x + j][stepsToBorder - 1] = counter;
                        counter++;
                    }
                    if (y == 2 && x < stepsToBorder - i && x != 0) {
                        array[stepsToBorder - 1][stepsToBorder - (x + 1)] = counter;
                        counter++;
                    }
                    if (y == 3 && x < stepsToBorder - (j + 1) && x != 0) {
                        array[stepsToBorder - (x + 1)][j] = counter;
                        counter++;
                    }
                }
            }
            stepsToBorder--;
            i++;
            j++;
        }
        return array;
    }

    public static void printMultyArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[1].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
        System.out.println();
    }
}