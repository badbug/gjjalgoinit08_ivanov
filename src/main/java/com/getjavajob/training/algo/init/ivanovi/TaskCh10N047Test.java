package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N047.*;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        findFibonacciTest();
    }

    public static void findFibonacciTest() {
        assertEquals("TaskCh10N047Test.findFibonacciTest", 13, findFibonacci(7));
    }
}