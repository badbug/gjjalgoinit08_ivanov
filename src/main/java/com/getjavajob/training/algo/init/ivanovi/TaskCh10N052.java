package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int reversed = 0;
        int reversedInt = reverseIntFunc(number, reversed);
        System.out.println(reversedInt);
    }

    public static int reverseIntFunc(int n, int reversed) {
        if (n < 10) {
            return reversed * 10 + n;
        }
        int tmp = reversed * 10 + n % 10;
        return reverseIntFunc(n / 10, tmp);
    }
}