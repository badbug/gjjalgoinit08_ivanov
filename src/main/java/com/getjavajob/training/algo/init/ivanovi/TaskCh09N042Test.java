package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N042.*;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        reversStringTest();
    }

    public static void reversStringTest() {
        assertEquals("TaskCh09N042Test.reversStringTest", "avaJ", reversString("Java"));
    }
}