package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7};
        int index = 0;
        System.out.println("Current array: " + printArray(array));
        int[] reversedArray = reverseArrayFunc(array, index);
        System.out.println("Reversed array: " + printArray(reversedArray));
    }

    public static int[] reverseArrayFunc(int[] array, int index) {
        int reversedIndex = array.length - index - 1;
        if (index >= reversedIndex) {
            return array;
        }
        swap(array, index, reversedIndex);
        return reverseArrayFunc(array, index + 1);
    }

    private static void swap(int[] array, int i1, int i2) {
        int tmp = array[i1];
        array[i1] = array[i2];
        array[i2] = tmp;
    }

    public static String printArray(int[] array) {
        String arrayToString = "";
        for (int i = 0; i < array.length; i++) {
            arrayToString += array[i] + " ";
        }
        return arrayToString;
    }
}