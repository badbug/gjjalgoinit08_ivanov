package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N185.*;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        checkBracketsATest();
        checkBracketsBTest();
    }

    public static void checkBracketsATest() {
        assertEquals("TaskCh09N185Test.checkBracketsATest", "yes", checkBracketsA("((x-1)*z + (x-2)*y) + (x-3)"));
    }

    public static void checkBracketsBTest() {
        assertEquals("TaskCh09N185Test.checkBracketsBTest", "Count of wrong open brackets = 1", checkBracketsB("(((x-1)*z + (x-2)*y) + (x-3)"));
    }
}