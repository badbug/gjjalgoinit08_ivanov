package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh11N245.*;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        deleteDuplicatesInArrayTest();
    }

    public static void deleteDuplicatesInArrayTest() {
        int[] array = {2, -2, -3, 1};
        int[] newArray = new int[array.length];
        int[] expectedArray = {-2, -3, 1, 2};
        assertEquals("TaskCh11N158Test.deleteDuplicatesInArrayTest", expectedArray, firstPositiveThenNegative(array, newArray));
    }
}