package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        boolean isEven = checkLastDigitIsEven(number1);
        boolean isOdd = checkLastDigitIsOdd(number2);
        System.out.println(isEven);
        System.out.println(isOdd);
    }

    public static boolean checkLastDigitIsEven(int number) {
        return number % 2 == 0;
    }

    public static boolean checkLastDigitIsOdd(int number) {
        return number % 2 != 0;
    }
}