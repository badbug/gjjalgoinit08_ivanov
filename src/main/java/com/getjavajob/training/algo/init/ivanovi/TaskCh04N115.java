package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int yearNumber = scanner.nextInt();
        if (yearNumber < 4) {                         //chinese calendar starts from 4th year of our era
            System.out.println("Invalid year");
        } else {
            String chinaYearName = checkChinaYear(yearNumber);
            System.out.println(chinaYearName);
        }
    }

    public static String checkChinaYear(int yearNumber) {
        int valueOfYear = yearNumber;
        while (valueOfYear >= 12) {
            valueOfYear %= 12;
        }
        String nameOfYear = "";
        switch (valueOfYear) {
            case (0):
                nameOfYear = "Monkey";
                break;
            case (1):
                nameOfYear = "Roster";
                break;
            case (2):
                nameOfYear = "Dog";
                break;
            case (3):
                nameOfYear = "Pig";
                break;
            case (4):
                nameOfYear = "Rat";
                break;
            case (5):
                nameOfYear = "Cow";
                break;
            case (6):
                nameOfYear = "Tiger";
                break;
            case (7):
                nameOfYear = "Rabbit";
                break;
            case (8):
                nameOfYear = "Dragon";
                break;
            case (9):
                nameOfYear = "Snake";
                break;
            case (10):
                nameOfYear = "Horse";
                break;
            case (11):
                nameOfYear = "Sheep";
                break;
            default:
                nameOfYear = "Unknown year name";
                break;
        }
        int valueOfColor = yearNumber;
        while (valueOfColor >= 10) {
            valueOfColor %= 10;
        }
        switch (valueOfColor) {
            case (0):
            case (1):
                nameOfYear += ", White";
                break;
            case (2):
            case (3):
                nameOfYear += ", Black";
                break;
            case (4):
            case (5):
                nameOfYear += ", Green";
                break;
            case (6):
            case (7):
                nameOfYear += ", Red";
                break;
            case (8):
            case (9):
                nameOfYear += ", Yellow";
                break;
            default:
                nameOfYear += ", unknown color";
                break;
        }
        return nameOfYear;
    }
}