package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N045.*;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        findSumOfArithmeticProgressionTest();
        findNOfArithmeticProgressionTest();
    }

    public static void findSumOfArithmeticProgressionTest() {
        assertEquals("TaskCh10N045Test.findSumOfArithmeticProgressionTest", 7, findSumOfArithmeticProgression(2, 3, 2));
    }

    public static void findNOfArithmeticProgressionTest() {
        assertEquals("TaskCh10N045Test.findNOfArithmeticProgressionTest", 5, findNOfArithmeticProgression(2, 3, 2));
    }
}