package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int startOfArithmeticProgression = scanner.nextInt();
        int stepOfArithmeticProgression = scanner.nextInt();
        int number = scanner.nextInt();
        int findSumOfArithmeticProgression = findSumOfArithmeticProgression(startOfArithmeticProgression, stepOfArithmeticProgression, number);
        int findNOfArithmeticProgression = findNOfArithmeticProgression(startOfArithmeticProgression, stepOfArithmeticProgression, number);
        System.out.println(findSumOfArithmeticProgression);
        System.out.println(findNOfArithmeticProgression);
    }

    public static int findSumOfArithmeticProgression(int startOfArithmeticProgression, int stepOfArithmeticProgression, int number) {
        if (number == 1) {
            return startOfArithmeticProgression;
        }
        return findSumOfArithmeticProgression(startOfArithmeticProgression + stepOfArithmeticProgression, stepOfArithmeticProgression, number - 1) + startOfArithmeticProgression;
    }

    public static int findNOfArithmeticProgression(int startOfArithmeticProgression, int stepOfArithmeticProgression, int number) {
        if (number == 1) {
            return startOfArithmeticProgression;
        }
        return findNOfArithmeticProgression(startOfArithmeticProgression, stepOfArithmeticProgression, number - 1) + stepOfArithmeticProgression;
    }
}