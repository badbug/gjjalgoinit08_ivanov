package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N022.*;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        getHalfOfWordTest();
    }

    public static void getHalfOfWordTest() {
        assertEquals("TaskCh09N017Test.getHalfOfWordTest", "Ja", getHalfOfWord("Java"));
    }
}