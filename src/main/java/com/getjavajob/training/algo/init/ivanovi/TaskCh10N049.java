package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh10N049 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 11, 112, 67, 54, 2};
        int counter = array.length;
        int maxElementIndex = 0;
        int maxElement = array[0];
        int maxElementIndexInArray = findMaxElementIndex(counter, array, maxElement, maxElementIndex);
        System.out.println(maxElementIndexInArray);
    }

    public static int findMaxElementIndex(int counter, int[] array, int maxElement, int maxElementIndex) {
        if (counter == 0) {
            return maxElementIndex;
        }
        if (array[counter - 1] > maxElement) {
            maxElement = array[counter - 1];
            maxElementIndex = counter - 1;
        }
        return findMaxElementIndex(counter - 1, array, maxElement, maxElementIndex);
    }
}