package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh09N166.*;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        reverseFirstAndLastWordsTest();
    }

    public static void reverseFirstAndLastWordsTest() {
        assertEquals("TaskCh09N166Test.reverseFirstAndLastWordsTest", "ramu mila Mama", reverseFirstAndLastWords("Mama mila ramu"));
    }
}