package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N046.*;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        findSumGeometricProgressionTest();
        findNinGeometricProgressionTest();
    }

    public static void findSumGeometricProgressionTest() {
        assertEquals("TaskCh10N046Test.findSumGeometricProgressionTest", 30, findSumGeometricProgression(2, 2, 4));
    }

    public static void findNinGeometricProgressionTest() {
        assertEquals("TaskCh10N046Test.findNinGeometricProgressionTest", 8, findNinGeometricProgression(2, 2, 3));
    }
}