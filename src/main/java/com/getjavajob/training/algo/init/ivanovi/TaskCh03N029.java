package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

/**
 * Task: write true condition when:
 * a) X and Y are odd
 * b) one of X and Y lesser than 20
 * c) X or Y equals zero
 * d) all of X, Y, Z lesser than zero
 * e) only one of X, Y, Z multiple of 5
 * f) one of X, Y, Z greater than 100
 * First we scan x,y,z from console, than call methods and print results
 */
public class TaskCh03N029 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();
        boolean allOdd = allOdd(x, y);
        boolean oneSmaller = oneSmaller(x, y);
        boolean oneZeroEqual = oneZeroEqual(x, y);
        boolean allSubZero = allSubZero(x, y, z);
        boolean onlyOneDivideByFive = onlyOneDivideByFive(x, y, z);
        boolean oneGreaterThanHundred = oneGreaterThanHundred(x, y, z);
        System.out.println(allOdd);
        System.out.println(oneSmaller);
        System.out.println(oneZeroEqual);
        System.out.println(allSubZero);
        System.out.println(onlyOneDivideByFive);
        System.out.println(oneGreaterThanHundred);
    }

    /**
     * call method allOdd(x,y) to check: X and Y are Odd
     *
     * @param x - inputed number
     * @param y - inputed number
     * @return boolean value
     */
    public static boolean allOdd(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * call method oneSmaller(x,y) to check: One of X and Y lesser than 20
     *
     * @param x - inputed number
     * @param y - inputed number
     * @return boolean value
     */
    public static boolean oneSmaller(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * call method oneZeroEqual(x,y) to check: X or Y equals zero
     *
     * @param x - inputed number
     * @param y - inputed number
     * @return boolean value
     */
    public static boolean oneZeroEqual(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * call method allSubZero(x,y,z) to check: All of X, Y, Z lesser than zero
     *
     * @param x - inputed number
     * @param y - inputed number
     * @param z - inputed number
     * @return boolean value
     */
    public static boolean allSubZero(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * call method oneDivideByFive(x,y,z) to check: Only one of X, Y, Z multiple of 5
     *
     * @param x - inputed number
     * @param y - inputed number
     * @param z - inputed number     *
     * @return boolean value
     */
    public static boolean onlyOneDivideByFive(int x, int y, int z) {
        boolean isXMultipleByFive = x % 5 == 0;
        boolean isYMultipleByFive = y % 5 == 0;
        boolean isZMultipleByFive = z % 5 == 0;

        return isXMultipleByFive && !isYMultipleByFive && !isZMultipleByFive
                || !isXMultipleByFive && isYMultipleByFive && !isZMultipleByFive
                || !isXMultipleByFive && !isYMultipleByFive && isZMultipleByFive;
    }

    /**
     * call method oneGreaterThanHundred(x,y,z) to check: One of X, Y, Z greater than 100
     *
     * @param x - inputed number
     * @param y - inputed number
     * @param z - inputed number
     * @return boolean value
     */
    public static boolean oneGreaterThanHundred(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}