package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh02N031.*;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        reverseSecondAndThirdCharTest();
    }

    public static void reverseSecondAndThirdCharTest() {
        assertEquals("TaskCh02N031Test.reverseSecondAndThirdCharTest", 132, reverseSecondAndThirdChar(123));
    }
}
