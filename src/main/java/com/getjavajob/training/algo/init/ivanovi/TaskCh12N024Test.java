package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh12N024.*;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        fillArrayTaskATest();
        fillArrayTaskBTest();
    }

    public static void fillArrayTaskATest() {
        int[][] array = new int[6][6];
        int[][] expectedArray = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.fillArrayTaskATest", expectedArray, fillArrayTaskA(array));
    }

    public static void fillArrayTaskBTest() {
        int[][] array = new int[6][6];
        int[][] expectedArray = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.fillArrayTaskBTest", expectedArray, fillArrayTaskB(array));
    }
}