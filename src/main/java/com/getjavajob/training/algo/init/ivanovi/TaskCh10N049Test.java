package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N049.*;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        findMaxElementIndexTest();
    }

    public static void findMaxElementIndexTest() {
        int[] array = {1, 334, 67, 98, 123, 777, 666, 7868};
        int counter = array.length;
        int maxElementIndex = 0;
        int maxElement = Integer.MIN_VALUE;
        assertEquals("TaskCh10N049Test.findMaxElementIndexTest", 7, findMaxElementIndex(counter, array, maxElement, maxElementIndex));
    }
}