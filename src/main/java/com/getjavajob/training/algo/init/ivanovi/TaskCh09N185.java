package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        String resultA = checkBracketsA(inputString);
        String resultB = checkBracketsB(inputString);
        System.out.println(resultA);
        System.out.println(resultB);
    }

    public static String checkBracketsA(String inputString) {
        int counter = 0;
        String result = "";
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == '(') {
                counter++;
            } else if (inputString.charAt(i) == ')') {
                counter--;
            }
        }
        if (counter == 0) {
            result = "yes";
        } else if (counter < 0 || counter > 1) {
            result = "no";
        }
        return result;
    }

    public static String checkBracketsB(String inputString) {
        int counter = 0;
        int countOpenBracket = 0;
        int countClosedBracket = 0;
        String result = "";
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == '(') {
                counter++;
                countOpenBracket++;
            } else if (inputString.charAt(i) == ')') {
                counter--;
                countClosedBracket++;
                if (counter < 0) {
                    result = "First wrong closed bracket position = " + i;
                }
            }
        }
        if (counter >= 1) {
            result = "Count of wrong open brackets = " + (countOpenBracket - countClosedBracket);
        } else if (counter == 0) {
            result = "yes";
        }
        return result;
    }
}