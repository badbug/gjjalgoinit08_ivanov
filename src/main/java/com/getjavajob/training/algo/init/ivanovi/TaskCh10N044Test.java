package com.getjavajob.training.algo.init.ivanovi;

import static com.getjavajob.training.algo.util.Assert.*;
import static com.getjavajob.training.algo.init.ivanovi.TaskCh10N044.*;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        digitalSqrtFuncTest();
    }

    public static void digitalSqrtFuncTest() {
        assertEquals("TaskCh10N044Test.digitalSqrtFuncTest", 3, digitalSqrtFunc(123456));
    }
}