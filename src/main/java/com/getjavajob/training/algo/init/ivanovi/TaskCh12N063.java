package com.getjavajob.training.algo.init.ivanovi;

public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] array = new int[][]{
                {10, 11, 13, 12},
                {11, 12, 13, 14},
                {11, 11, 11, 11},
                {12, 14, 16, 20},
                {22, 25, 25, 27},
                {19, 16, 21, 16},
                {12, 11, 15, 12},
                {11, 12, 13, 14},
                {24, 23, 22, 21},
                {12, 14, 16, 20},
                {22, 25, 25, 27}
        };
        double[] avgPupilCount = avgPupilCountFunc(array);
        printArray(avgPupilCount);
    }

    public static double[] avgPupilCountFunc(int[][] array) {
        double[] avgPupilCount = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            int avgCount = 0;
            for (int j = 0; j < array[1].length; j++) {
                avgCount += array[i][j];
            }
            double classesCount = array[1].length;
            avgPupilCount[i] = avgCount / classesCount;
        }
        return avgPupilCount;
    }

    public static void printArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Average pupil in " + (i + 1) + " class = " + array[i]);
        }
    }
}