package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter word: ");
        String word = scanner.nextLine();
        System.out.print("Enter number of symbol: ");
        int charNumber = scanner.nextInt();
        char charAtNPosition;
        if (charNumber > 0 && charNumber <= word.length()) {
            charAtNPosition = getCharAtNPosition(word, charNumber);
            System.out.println(charAtNPosition);
        } else {
            System.out.println("Incorrect symbol position");
        }
    }

    public static char getCharAtNPosition(String word, int n) {
        return word.charAt(n - 1);
    }
}