package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        String reversedString = reversString(inputString);
        System.out.println(reversedString);
    }

    public static String reversString(String inputString) {
        StringBuilder reversedString = new StringBuilder(inputString);
        reversedString.reverse();
        return reversedString.toString();
    }
}