package com.getjavajob.training.algo.init.ivanovi;

import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int divider = 2;
        boolean isSimple = isSimpleCheck(number, divider);
        System.out.println(isSimple);
    }

    public static boolean isSimpleCheck(int number, int divider) {
        if (divider + 1 == number || number == 2) {
            return true;
        }
        if (number % divider == 0) {
            return false;
        }
        return isSimpleCheck(number, divider + 1);
    }
}