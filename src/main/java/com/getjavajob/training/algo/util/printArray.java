package com.getjavajob.training.algo.util;

public class PrintArray {

    public static String printArray(int[] array) {
        String arrayToString = "";
        for (int i = 0; i < array.length; i++) {
            arrayToString += array[i] + " ";
        }
        return arrayToString;
    }
}